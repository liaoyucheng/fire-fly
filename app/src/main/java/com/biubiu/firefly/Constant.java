package com.biubiu.firefly;

public class Constant {
    //物理盒子
    public static float TIME_STEP = 6f/60f; //模拟世界的频率
    public static int VELOCITY = 5; //速率迭代器
    public static int ITERATE = 20; //迭代次数
    public static int PROPORTION = 10;//模拟世界与屏幕的比例
    public static float ACCELERATION = 10f;//acceleration

}
