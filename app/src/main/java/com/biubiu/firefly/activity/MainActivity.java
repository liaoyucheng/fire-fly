package com.biubiu.firefly.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.biubiu.firefly.wallpaper.box.BoxWallpaperService;
import com.biubiu.firefly.wallpaper.firefly.FireFlyWallpaperService;
import com.biubiu.firefly.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_go).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("android.service.wallpaper.CHANGE_LIVE_WALLPAPER");
                intent.putExtra("android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT",
                        new ComponentName(MainActivity.this, FireFlyWallpaperService.class));
                MainActivity.this.startActivity(intent);
            }
        });
        findViewById(R.id.btn_box).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BoxSettingsActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });
        findViewById(R.id.btn_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BoxSettingsActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

    }
}