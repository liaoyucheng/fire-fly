package com.biubiu.firefly.wallpaper.box

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AttributeBody(
    val isRandomRadius: Boolean = false,
    val radius: Int = 50,
    val gravity: Int = 10,
    val frictional: Float = 0.8f,
    val elasticity: Float = 0.6f,
    val bitmapList: List<Bitmap>
) : Parcelable
