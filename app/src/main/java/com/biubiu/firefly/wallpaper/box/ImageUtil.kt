package com.biubiu.firefly.wallpaper.box

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint

object ImageUtil{

    /**
     * 两张图片叠层
     */
    fun drawOverlapBitmap(bitmapA: Bitmap, bitmapB: Bitmap): Bitmap {
        // 得到图片的宽、高
        val widthA: Int = bitmapA.width
        val heightA: Int = bitmapA.height
        val widthB: Int = bitmapB.width
        val heightB: Int = bitmapB.height

        // 创建一个你需要尺寸的Bitmap
        val mBitmap = Bitmap.createBitmap(widthA, heightA, Bitmap.Config.ARGB_8888)
        // 用这个Bitmap生成一个Canvas,然后canvas就会把内容绘制到上面这个bitmap中
        val canvas = Canvas(mBitmap)
        val bitmapBPaint = Paint()

        if(bitmapB != null){
            canvas.drawBitmap(bitmapB,(widthA - widthB) / 2f,(heightA - heightB) / 2f, bitmapBPaint)
        }

        if(bitmapA != null){
            canvas.drawBitmap(bitmapA,0f,0f, bitmapBPaint)
        }

        canvas.save()
        canvas.restore()
        return mBitmap
    }
}
