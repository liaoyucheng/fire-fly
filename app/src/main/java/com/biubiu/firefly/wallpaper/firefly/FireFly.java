package com.biubiu.firefly.wallpaper.firefly;

import android.content.Context;
import android.util.DisplayMetrics;

import java.util.Random;

/**
 * 萤火虫属性
 */
public class FireFly {
    private int diameter;//决定体型大型,直径
    private float speed;//每帧飞行距离
    private int plusMinusX;//只有-1和1两个值，表示正负,决定x轴运动方向
    private int angle;//[0,360]//线性运动方向与x轴逆时针夹角
    private int alpha;//透明度[0,255]
    private int initAlpha;//变化的透明度
    private float ffx;//x轴坐标
    private float ffy;//y轴坐标
    private int disTime;//每次闪烁间隔时间
    private long twinkleTime;//下一次闪烁的时间戳
    private boolean isShine;//亮起或熄灭

    public FireFly(Context context) {
        changeData(context);
    }

    public void changeData(Context context){
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Random rd = new Random();
        diameter = 18 + rd.nextInt(5)*2;
        speed = 0.5f + rd.nextFloat()*1.0f;
        plusMinusX = rd.nextBoolean()?1:-1;
        angle = rd.nextInt(360);
        alpha = 10+rd.nextInt(7);
        initAlpha = 0;
        disTime = 2000 + rd.nextInt(5000);
        twinkleTime = System.currentTimeMillis() + disTime;
        isShine = true;

        ffx = rd.nextInt(displayMetrics.widthPixels);
        ffy = rd.nextInt(displayMetrics.heightPixels);
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getPlusMinusX() {
        return plusMinusX;
    }

    public void setPlusMinusX(int plusMinusX) {
        this.plusMinusX = plusMinusX;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public float getFfx() {
        return ffx;
    }

    public void setFfx(float ffx) {
        this.ffx = ffx;
    }

    public float getFfy() {
        return ffy;
    }

    public void setFfy(float ffy) {
        this.ffy = ffy;
    }

    public int getDisTime() {
        return disTime;
    }

    public void setDisTime(int disTime) {
        this.disTime = disTime;
    }

    public long getTwinkleTime() {
        return twinkleTime;
    }

    public void setTwinkleTime(long twinkleTime) {
        this.twinkleTime = twinkleTime;
    }

    public int getInitAlpha() {
        return initAlpha;
    }

    public void setInitAlpha(int initAlpha) {
        this.initAlpha = initAlpha;
    }

    public boolean isShine() {
        return isShine;
    }

    public void setShine(boolean shine) {
        isShine = shine;
    }
}
