package com.biubiu.firefly.wallpaper.box;

import android.graphics.Bitmap;

import java.util.List;

public class WorldFeatures {
    //重力加速度
    public int gravity = 10;
    //摩擦系数
    public float friction = 0.8f;
    //弹性系数
    public float restitution = 0.6f;
    //刚体半径
    public float radius = 125f;
    //刚体bitmap列表
    public List<Bitmap> stickers;
    //随机半径
    public boolean randomRadius = false;

    private static WorldFeatures worldFeatures;

    private WorldFeatures() {
    }

    public static WorldFeatures getWorldFeatures() {
        if (worldFeatures == null) {
            worldFeatures = new WorldFeatures();
        }
        return worldFeatures;
    }

    public int getGravity() {
        return gravity;
    }

    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    public float getFriction() {
        return friction;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }

    public float getRestitution() {
        return restitution;
    }

    public void setRestitution(float restitution) {
        this.restitution = restitution;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public List<Bitmap> getStickers() {
        return stickers;
    }

    public void setStickers(List<Bitmap> stickers) {
        this.stickers = stickers;
    }

    public static void setWorldFeatures(WorldFeatures worldFeatures) {
        WorldFeatures.worldFeatures = worldFeatures;
    }

    public boolean isRandomRadius() {
        return randomRadius;
    }

    public void setRandomRadius(boolean randomRadius) {
        this.randomRadius = randomRadius;
    }
}
