package com.biubiu.firefly.wallpaper.box;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import com.biubiu.firefly.Constant;

import org.jbox2d.dynamics.Body;

public class MyCircleColor extends MyBody {
    float radius; //圆形半径

    Bitmap stickers;//圆形贴纸图片

    @Override
    public void drawSelf(Canvas canvas, Paint paint) {
        body.setAwake(true);//设置刚体为活跃状态
        float cx=switchPositionToView(body.getPosition().x);
        float cy=switchPositionToView(body.getPosition().y);

        if(stickers != null){
            float left = cx-radius;
            float top = cy-radius;
            canvas.drawBitmap(stickers, left, top,null);
        }else {
            //抗锯齿
            paint.setAntiAlias(true);
            //画圆
            paint.setColor(color&0xAFFFFFFF); //设置颜色
            paint.setStyle(Style.FILL); //填充
            canvas.drawCircle(cx, cy, radius, paint);
            //画边
            paint.setColor(color);
            paint.setStyle(Style.STROKE); //设置空心无填充
            paint.setStrokeWidth(1);
            canvas.drawCircle(cx, cy, radius, paint);
        }

        paint.reset(); //恢复画笔设置
    }

    public MyCircleColor(Body body,float radius,int color,Bitmap stickers) {
        this.body=body;
        this.radius=radius;
        this.color=color;
        this.stickers=stickers;
    }

    //物理的坐标映射为view坐标
    private float switchPositionToView(float bodyPosition) {
        return bodyPosition * Constant.PROPORTION;
    }
}
