package com.biubiu.firefly.wallpaper.box;

import org.jbox2d.dynamics.Body;
import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class MyBody {
    Body body;  //JBox2D物理引擎中的刚体
    int color; //刚体的颜色
    public abstract void drawSelf(Canvas canvas,Paint paint); //绘制的方法
    public void setAwake(boolean b){
        body.setAwake(b);
    }
}
